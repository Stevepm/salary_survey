class UserMailer < ActionMailer::Base
  default from: "test@salarysurvey.com"

  def survey_notification(survey, result)
    user = result.user
    @survey = survey

    @url = survey_result_url(survey_id: survey.id, id: result.id, token: result.token)
    mail(to: user.email, subject: 'Salary Survey')
  end

  def results_notification(survey, result)
    @survey = survey

    @url = results_url(survey_id: survey.id, id: result.id, token: result.viewer_token)
    mail(to: result.user.email, subject: 'Salary Survey 2')
  end
end
