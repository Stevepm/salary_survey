require 'digest'

class User < ActiveRecord::Base
  has_many :results
  validates :email, format: {
    :with => /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
    :message => "%{value} is not valid",
    :multiline => true
  }

  def self.validate_addresses(emails)
    errors = []
    emails.each do |email|
      user = new email: email
      errors << user.errors.full_messages.join(',') unless user.valid?
    end
    errors
  end
end
