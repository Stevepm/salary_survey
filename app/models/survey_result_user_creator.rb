module SurveyResultUserCreator
  module_function

  def create(survey, users)
    users.each do |user|
      user.save!
      Result.create!(survey: survey, user: user)
    end
  end
end

# controller:
#SurveyResultUserCreator.create survey, emails

#include SurveyResultUserCreator
#create ...