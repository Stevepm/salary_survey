class SurveyNotifier
  def initialize(survey)
    @survey = survey
  end

  def notify!
    @survey.results.each do |result|
      UserMailer.survey_notification(@survey, result).deliver
    end
  end

  def notify_results!
    @survey.results.each do |result|
      UserMailer.results_notification(@survey, result).deliver
    end
  end
end