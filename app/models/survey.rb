class Survey < ActiveRecord::Base
  self.primary_key = :id

  has_many :results
  has_many :users, :through => :results
  accepts_nested_attributes_for :users

  validates :id, uniqueness: true
  validates :company_name, presence: true
  validates :job_title, presence: true
  validate :min_number_of_results

  before_create :set_expiration_date

  def results_entered?
    results.all? { |result| result.complete }
  end

  def min_number_of_results
    errors.add(:you, ' must enter a minimum of 5 email addresses') if results.size < 5
  end

  def set_expiration_date
    self.expires_at = 7.days.from_now # before_filter
  end

end
