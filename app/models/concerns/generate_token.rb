module GenerateToken
  extend ActiveSupport::Concern

  included do
    before_create :generate_token
  end

  def generate_token
    hasher = Digest::SHA256.new
    self.token = hasher.hexdigest "#{ Time.new.strftime '%H %M %S %L %9N'} iashdlfkjhasdlkjhiuaweytr"
    self.viewer_token = hasher.hexdigest "#{ Time.new.strftime '%H %M %S %L %9N'} jjkawjdjkawdkjawdkjk"
  end
end