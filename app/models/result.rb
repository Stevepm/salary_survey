require 'securerandom'

class Result < ActiveRecord::Base
  self.primary_key = :id
  include GenerateToken

  belongs_to :survey
  belongs_to :user

  validates :id, uniqueness: true

  validates :salary, format: { with: /[^a-zA-Z]/,
                                    message: "only allows numbers" , on: :update}

  before_create :set_uuid


  def set_uuid
    self.id = SecureRandom.uuid
  end

  def salary=(num)
    if !(num =~ /[[:alpha:]]/)
      num.gsub!(',','') if num.is_a?(String)
      self[:salary] = num.to_i
    end
  end
end
