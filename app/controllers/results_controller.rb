class ResultsController < ApplicationController
  before_action :set_result, only: [:show, :update, :view]

  def view
    @survey = Survey.find(params[:survey_id])
    @salaries = get_results(@survey)
    token = params[:token]

    if @result.viewer_token != token || token.nil? || @result.result_viewed
      render status: :not_found, layout: '404'
    else
      @result.update(:result_viewed => true)
    end

  end

  def show
    @survey = Survey.find(params[:survey_id])
    token = params[:token]

    if @result.token != token || token.nil? || @result.complete
      render status: :not_found, layout: '404'
    end
  end

  def update
    token = result_params[:token]
    @survey = Survey.find(params[:survey_id])

    if @result.token != token || token.nil? || @result.complete
      render status: :not_found, layout: '404'
    elsif @result.update(result_params.merge(complete: true))
      if @survey.results_entered?
        SurveyNotifier.new(@survey).notify_results!
      end
      render :thanks
    else
      render :show
    end
  end

  def thanks

  end

  private
  def set_result
    @result = Result.find(params[:id])
  end

  def result_params
    params.require(:result).permit(:salary, :token)
  end

  def get_results(survey)
    survey.results.map(&:salary).each_with_index.map do |salary, index|
      ["Recipient #{index + 1}", salary]
    end.sort.uniq
  end

end
