require 'securerandom'

class WelcomeController < ApplicationController

  def index
    @survey = Survey.new
    @survey_id = SecureRandom.uuid
  end

end