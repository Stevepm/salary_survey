class SurveysController < ApplicationController
  # POST /surveys
  # POST /surveys.json
  def create
    @users, @user_errors = [], []
    @survey = Survey.new(survey_params)

    email_params.each do |email|
      @users << (user = User.new(email: email))
      @survey.results << Result.new(user: user)
    end

    @survey.save!

    SurveyNotifier.new(@survey).notify!
  rescue ActiveRecord::Rollback, ActiveRecord::RecordInvalid
    @survey_id = @survey.id
    collect_user_errors
    render 'welcome/index'
  end

  private

# Never trust parameters from the scary internet, only allow the white list through.
  def survey_params
    params.permit(:company_name, :job_title, :id)
  end

  def email_params
    return @email_params if @email_params
    email_params = params.permit(:email_ui, :emails => [])
    @email_params = [email_params[:emails], email_params[:email_ui]].flatten.compact.reject(&:blank?)
  end

  def collect_user_errors
    @user_errors += @users.map {|user| user.errors.full_messages.join(',') unless user.valid? }.compact
  end
end
