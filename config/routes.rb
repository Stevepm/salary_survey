Rails.application.routes.draw do

  get '/', to: 'welcome#index'

  resources :users

  resources :surveys do
    resources :results
  end

  get '/surveys/:survey_id/results/:id/view', to: 'results#view', as: 'results'

end
