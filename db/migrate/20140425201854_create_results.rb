class CreateResults < ActiveRecord::Migration
  def change
    create_table :results, id: false do |t|
      t.string :id, null: false
      t.string :survey_id
      t.integer :user_id
      t.integer :salary
      t.string :token
      t.boolean :complete, default: false
      t.string :viewer_token
      t.boolean :result_viewed, default: false

      t.timestamps
    end
  end
end
