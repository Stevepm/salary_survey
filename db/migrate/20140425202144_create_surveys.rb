class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys, id: false do |t|
      t.string :id, null: false
      t.string :company_name
      t.time :expires_at
      t.string :job_title

      t.timestamps
    end
  end
end
