require 'spec_helper'

feature "Happy path" do
  before do
    ActionMailer::Base.deliveries = []
    # Capybara.current_driver = :selenium


  end


  scenario 'user can update their salary information', js: true do

    visit '/'
    fill_in 'Company name', with: 'Company test'
    fill_in 'Job title', with: 'Job test'

    fill_in 'email_ui', with: 'email1@test.com'
    find('#email_ui').native.send_keys(:return)
    fill_in 'email_ui', with: 'email1@test.com'
    find('#email_ui').native.send_keys(:return)

    fill_in 'email_ui', with: 'email1@test.com'
    find('#email_ui').native.send_keys(:return)

    fill_in 'email_ui', with: 'email1@test.com'
    find('#email_ui').native.send_keys(:return)

    click_on "Create Survey"
    expect(page).to have_content('You must enter a minimum of 5 email addresses')

    fill_in 'email_ui', with: 'email1@test.com'
    find('#email_ui').native.send_keys(:return)

    click_on "Create Survey"

    within('li#company') do
      expect(find('strong')).to have_content('Company test')
    end
    within('li#job') do
      expect(find('strong')).to have_content('Job test')
    end

    expect(page).to have_content("Sucess! The survey has been created.")

    expect(ActionMailer::Base.deliveries.length).to be(5)

    survey = Survey.order(:created_at).last

    survey.results.each do |result|
      visit "/surveys/#{survey.id}/results/#{result.id}?token=#{result.token}"
      expect(find(:xpath, '//input[@id = "company-name"]').value).to eq("#{survey.company_name}")
      expect(find(:xpath, '//input[@id = "job-title"]').value).to eq("#{survey.job_title}")
      fill_in 'result_salary', with: '123kjkj123'
      expect(page).to have_content('Please enter an integer only')
      fill_in 'result_salary', with: '100,000'
      click_on 'Submit salary'
      expect(page).to have_content('Your salary has been submitted.
      We will email you when the final results are ready to be viewed.')
      result.reload
      expect(result.salary).to eq(100000)
      expect(result.complete).to eq(true)
      visit "/surveys/#{survey.id}/results/#{result.id}?token=#{result.token}"
      expect(page).to have_content("Sorry, but the page you are looking for was not found.")
    end

    expect(ActionMailer::Base.deliveries.length).to be(10)

    survey.results.each do |result|
      visit "/surveys/#{survey.id}/results/#{result.id}/view?token=#{result.viewer_token}"
      expect(page).to have_no_content('Sorry, but the page you are looking for was not found.')
    end
    survey.results.each do |result|
      visit "/surveys/#{survey.id}/results/#{result.id}/view?token=#{result.viewer_token}"
      expect(page).to have_content('Sorry, but the page you are looking for was not found.')
    end
  end


  scenario 'user sees 404 if visiting a result page with invalid token' do
    visit "/surveys/5/results/6?token=INVALID"
    expect(page).to have_content("404 Not Found")
  end
end