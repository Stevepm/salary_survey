require 'spec_helper'

describe User do


  it 'displays an error if email address isnt given' do
    user = User.new
    user.save
    expect(user.errors.full_messages).to eq(['Email  is not valid'])

    user = User.new(:email => '')
    user.save
    expect(user.errors.full_messages).to eq(['Email  is not valid'])
  end
end
