require 'spec_helper'

describe SurveyNotifier do
  let (:survey) { Survey.last }
  describe 'survey notifier' do

    let(:mail) { SurveyNotifier.new(survey).notify! }

    it 'sends mail out to people' do
      expect(mail.length).to eq(survey.results.count)
    end
  end

  describe 'results notifier' do

    let(:mail) { SurveyNotifier.new(survey).notify_results! }

    it 'sends mail out to people' do
      expect(mail.length).to eq(survey.results.count)
    end

  end


end