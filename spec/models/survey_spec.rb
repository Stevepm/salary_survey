require 'spec_helper'

describe Survey do
  it "gives an error if no company name or job title is given" do
    survey = Survey.new(:job_title => 'hh')
    survey.save

    expect(survey.errors.full_messages).to eq(["Company name can't be blank", "You  must enter a minimum of 5 email addresses"])

    survey = Survey.new(:company_name => 'hh')
    survey.save

    expect(survey.errors.full_messages).to eq(["Job title can't be blank", "You  must enter a minimum of 5 email addresses"])
  end

end
