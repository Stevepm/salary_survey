require 'spec_helper'

describe Result do
  let (:survey) { survey = Survey.new(company_name: 'company', job_title: 'job', id: SecureRandom.uuid) }
  it "creates a result with a token hash" do
    result = Result.new(:user_id => 1, :survey => survey, :salary => '1000')
    result.save
    expect(result.valid?).to eq(true)
    expect(result.token.present?).to eq(true)
    expect(result.viewer_token.present?).to eq(true)
    expect(result.complete).to eq(false)
    expect(result.result_viewed).to eq(false)
  end

  it 'displays an error if trying to update salary without a number' do
    result = Result.create(:user_id => 1, :survey => survey)

    result.update(salary: 'hello')

    expect(result.errors.full_messages).to eq(["Salary only allows numbers"])
  end
end
