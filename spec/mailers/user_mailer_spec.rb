require "spec_helper"

describe UserMailer do
  let(:survey) { Survey.create(company_name: 'test company name', job_title: 'test job title', id: SecureRandom.uuid) }
  let(:user) { User.create(email: 'steven.magelowitz@gmail.com') }
  let(:result) { Result.create(user: user, survey: survey) }
  describe 'survey notification' do
    let(:mail) { UserMailer.survey_notification(survey, result) }
    let (:page) { Capybara.string(mail.body.to_s) }
    it 'renders the subject' do
      expect(mail.subject).to eq('Salary Survey')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq(['steven.magelowitz@gmail.com'])
    end

    it 'has the correct company name' do
      expect(page).to have_content('test company name')
    end

    it 'has the correct job title' do
      expect(page).to have_content('test job title')
    end
  end

  describe 'results notification' do
    let(:mail) { UserMailer.results_notification(survey, result) }
    let (:page) { Capybara.string(mail.body.to_s) }
    it 'renders the subject' do
      expect(mail.subject).to eq('Salary Survey 2')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq(['steven.magelowitz@gmail.com'])
    end

    it 'has the correct company name' do
      expect(page).to have_content('test company name')
    end
  end
end
